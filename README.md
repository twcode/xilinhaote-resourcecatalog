### 目录

* [简介](#abstract)
* [版本记录](#version)

---

### <a name="abstract">简介</a>

用于内蒙古-锡林浩特市资源目录信息配置

---

### <a name="version">版本记录</a>

* [1.0.0](./Docs/Version/0.1.0.md "1.0.0")
